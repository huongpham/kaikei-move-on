package fairy.tail;

import fairy.tail.practice.Book;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Long a = 4l;
        Long b = null;
        boolean rs = a.equals(b);
        System.out.println(rs);
        int fib = getNthFib(2);
        System.out.println(fib);

//        study java8
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("The Fellowship of the Ring", 1954, "0395489318"));
        bookList.add(new Book("The Two Towers", 1954, "0345339711"));
        bookList.add(new Book("The Return of the King", 1955, "0618129111"));
        Map map = listToMap(bookList);
        map.forEach((k, v) -> System.out.println("Key: " + k + " Value: " + v ));
    }
    static int getNthFib(int n) {
        if (n == 2) {
            return 1;
        } else if (n == 1 || n <= 0) {
            return  0;
        }
        return getNthFib(n-1) + getNthFib(n -2);
    }

    public static Map<Integer, Book> listToMap(List<Book> books) {
        return books.stream().collect(Collectors.toMap(Book::getReleaseYear, v -> v, (v, q) -> q));
    }

}
