package fairy.tail.thread;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConcurrencyLockExample implements Runnable {

    private Lock lock;

    public ConcurrencyLockExample(){
        this.lock = new ReentrantLock();
    }

    @Override
    public void run() {
        try {
            if(lock.tryLock(10, TimeUnit.MILLISECONDS)){
                doSomething();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally{
            //release lock
            lock.unlock();
            System.out.println("unlock");
        }
        doLogging();
    }

    private void doLogging() {
        System.out.println("run end!! <3");
    }

    private void doSomething() {
        while (true)
            System.out.println("Duoling Ms.Huong");
    }

    public static void main(String[] args) {
//        run not well
        ConcurrencyLockExample lockExample = new ConcurrencyLockExample();
        lockExample.run();
    }
}
