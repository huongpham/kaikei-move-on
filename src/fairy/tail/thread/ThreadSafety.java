package fairy.tail.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadSafety {

    public static void main(String[] args) throws InterruptedException {
        ProcessingThread pt = new ProcessingThread();
        Thread t1 = new Thread(pt, "t1");
        Thread t2 = new Thread(pt, "t2");
        Thread t3 = new Thread(pt, "t3");
        ExecutorService poolExecutor = Executors.newFixedThreadPool(2);
        poolExecutor.execute(t1);
        poolExecutor.execute(t2);
        poolExecutor.execute(t3);
        poolExecutor.shutdown();
//        t3.start();
//        t3.join();
//        t2.start();
//        t2.join();
//        t1.start();
//        t1.join();
        System.out.println("Processing count="+pt.getCount());

    }
}

class ProcessingThread implements Runnable {
    private int count;

    @Override
    public void run() {
        for (int i = 1; i < 5; i++) {
            processSomething(i);
            count++;
            System.out.println("current thread: "+Thread.currentThread().getName()+ "--i: "+i);
        }
    }

    private void processSomething(int i) {
        try {
            Thread.sleep(i*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public int getCount() {
        return count;
    }
}

class ThreadJoining extends Thread
{
    @Override
    public void run()
    {
        for (int i = 0; i < 2; i++)
        {
            try
            {
                Thread.sleep(500);
                System.out.println("Current Thread: "
                        + Thread.currentThread().getName());
            }

            catch(Exception ex)
            {
                System.out.println("Exception has" +
                        " been caught" + ex);
            }
            System.out.println(i);
        }
    }
}