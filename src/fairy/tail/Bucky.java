package fairy.tail;

public class Bucky {

    public static void main(String[] args) {
        Integer[] iray = {1,2,3,4,5,5};
        Character[] cray = {'b','u', 'c', 'k', 'y'};

        printMe(iray);
        printMe(cray);
    }

    public static <ex> void printMe(ex[] x) {
        for (ex b : x) {
            System.out.printf("%s ", b);
        }
        System.out.println();
    }
}
