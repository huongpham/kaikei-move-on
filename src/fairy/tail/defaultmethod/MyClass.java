package fairy.tail.defaultmethod;

public class MyClass implements Interface1, Interface2 {
    @Override
    public void method1() {

    }

    @Override
    public void log(String str) {
        Interface1.print("");
    }

    @Override
    public void method2() {

    }

    public static void main(String[] args) {
        MyClass myClass = new MyClass();
        System.out.println("I implemented: " +myClass.getClass().getInterfaces()[0].getName());
        myClass.log("test");
        String hello = "hello";
        String hello2 = "hello";
        System.out.println(hello == hello2);
        MathOperation addition = (a, b) -> a + b;
        System.out.println(addition.operation(5, 2));
    }
}
