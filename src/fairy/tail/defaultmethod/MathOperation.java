package fairy.tail.defaultmethod;

public interface MathOperation {

    int operation(int a, int b);
}
