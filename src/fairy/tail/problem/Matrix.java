package fairy.tail.problem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Matrix {

    public static void main(String[] args) {

        int[][] mat = {
                {1, 3, 5, 8},
                {10, 11, 15, 16},
                {24, 27, 30, 31},
        };
        System.out.println(binarySearchColumn(mat, 27));
        int arr[] = {2,4,1,5,3,5,2,4,88, 88, 99, 44};
        System.out.println(binarySearch(arr, 4));
//        next
        String input = "2 3 4 56 7";
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        List<List<Integer>> arrs = new ArrayList<>();
        IntStream.range(0, n).forEach(i -> {
            arrs.add(
                    Stream.of(input.replaceAll("\\s+$", "").split(" "))
                            .map(Integer::parseInt)
                            .collect(Collectors.toList())
            );
        });
        System.out.println("array input: " +arrs.toString());
        int result = diagonalDifference(arrs);
        System.out.println("result different: "+result);
    }

    public static int diagonalDifference(List<List<Integer>> arr) {
        // Write your code here
//        arr.forEach(i ->  {
//            System.out.print("matrix 1d: " + i);
//            System.out.println();
//            i.forEach(j -> System.out.print("element: " +j));
//        });
        System.out.println("size arr: "+arr.get(0).get(2));
        int totalLeft = 0;
        int totalRight = 0;
        for (int i = 0; i < arr.size(); i++) {
            totalLeft += arr.get(i).get(i);
        }
        int x = 0;
        for (int i = arr.get(0).size() - 1; i >= 0; i--) {
            System.out.println("debug position: " +x+ "--" +i);
            totalRight += arr.get(x).get(i);
            x++;
        }

        System.out.println("debug size arr: "+arr.get(2).size());
        System.out.println("total left: "+totalLeft+ "-- total right: "+totalRight);
        return Math.abs(totalLeft - totalRight);
    }

    /**
     * not yet use bst
     * @param mat
     * @param target
     * @return
     */
    private static boolean binarySearchColumn(int[][] mat, int target) {
        int rowTarget = -1;
        for (int i = 0; i < mat[0].length - 1; i++) {
//            System.out.println("-debug--: i, 0: " + mat[i][0] + "--i, max: "+mat[i][mat.length]);
            if (target >= mat[i][0] && target <= mat[i][mat.length - 1]) {
                rowTarget = i;
                break;
            }
        }
        System.out.println(rowTarget + "--debug");
        if (rowTarget < 0)
            return false;

        for (int i = 0; i < mat.length; i++) {
            if (target == mat[rowTarget][i])
                return true;
        }
        return false;
    }
    private static int binarySearch(int arr[], int x)
    {
        int l = 0, r = arr.length - 1;
        while (l <= r) {
            int m = l + (r - l) / 2;

            // Check if x is present at mid
            if (arr[m] == x)
                return m;

            // If x greater, ignore left half
            if (arr[m] < x)
                l = m + 1;

                // If x is smaller, ignore right half
            else
                r = m - 1;
        }

        // if we reach here, then element was
        // not present
        return -1;
    }

}
