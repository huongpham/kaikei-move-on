package fairy.tail.problem;

import java.util.*;
import java.util.stream.Collectors;

public class Geeks {

    public static void main(String[] args) {
//        findTriplets(new int[]{0, -1, 2, -3, 1}, 5);
//        checkPrime();
        System.out.println(checkPerfectNumber(99999999));
    }

    // solution not good, find couple with value = -a -> total = 0
    private static void findTriplets(int[] arr, int n) {
        for (int i = 0; i < n - 2; i++) {
            for (int j = i + 1; j < n - 1; j++) {
                int total = arr[i] + arr[j];
                List<Integer> thirds = Arrays.stream(arr).boxed().filter(a -> a + total == 0).collect(Collectors.toList());
                if (thirds.size() > 0 && thirds.get(0) != arr[i] && thirds.get(0) != arr[j]) {
                    System.out.println("find triplets: " + arr[i] + ", " + arr[j] + ", " + thirds.get(0));
                    continue;
                }

            }
        }
    }

    //A prime is a natural number greater than 1 that has no positive divisors other than 1 and itself.
    // Given a number n, determine and print whether it's Prime  or Not prime.
    // my solution is so slow, cost CPU.. test: 1000000000 - 1000000009 --> sqrt is best solution 0(n)
    private static void checkPrime() {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        for (int i = 0; i < n; i++) {
            int input = scan.nextInt();
            boolean isPrime = true;
            for (int j = 2; j*j <= input; j++) {
                if (input % j == 0) {
                    System.out.println("Not prime");
                    isPrime = false;
                    break;
                } else {

                    continue;
                }
            }
            if (isPrime) {
                System.out.println("Prime");
            }
        }
    }

    private static void singletonList() {
        List single = Collections.singletonList("sington list");
        System.out.println(single);
    }

//    Solution (Optimal) N^(.5) T | 1 Space
    public static boolean checkPerfectNumber(int num) {
        if (num == 0 || num == 1) return false;
        int res = 1;
        for (int i = 2; i * i < num; i++) {
            if (num % i == 0) {
                res += i;
                res += num/i;
            }
        }
        return res == num;
    }
}
