package fairy.tail.problem;

import fairy.tail.generic.Printer;

import java.text.DecimalFormat;
import java.util.*;

public class HackerRank {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] arr = {2, 3, 2, 94, 91, 94, 99, 91, 91};
        System.out.println(sockMerchant(arr));
    }

    /**
     * count total pair sock
     * @param ar
     * @return
     */
    static int sockMerchant(int[] ar) {
        Arrays.parallelSort(ar);
        int totalPair = 0;
        int preNum = 0;
        for (int h : ar) {
            if (preNum == 0) {
                preNum = h;
            } else {
                if (preNum == h) {
                    totalPair++;
                    preNum = -1;
                } else {
                    preNum = h;
                }
            }
        }
        return totalPair;
    }
    /**
     * Array is sorted in ? swaps.
     * First Element: ?
     * Last Element: ?
     * @param a
     */
    static void sorting(int[] a) {
//        int[] sort = Arrays.stream(n).sorted().toArray();
//        System.out.println(Arrays.toString(sort));
        int n = a.length;
        int numberOfSwaps = 0;
        for (int i = 0; i < n; i++) {
            // Track number of elements swapped during a single array traversal

            for (int j = 0; j < n - 1; j++) {
                // Swap adjacent elements if they are in decreasing order
                if (a[j] > a[j + 1]) {
                    System.out.println(a[j]+"-"+ a[j+1]);
                    a[j] = a[j] ^ a[j+1];
                    a[j+1] = a[j+1] ^ a[j];
                    a[j] = a[j] ^ a[j+1];
                    numberOfSwaps++;
                }
            }

            // If no elements were swapped during a traversal, array is sorted
            if (numberOfSwaps == 0) {
                break;
            }
        }
        System.out.println(String.format("Array is sorted in %s swaps.", numberOfSwaps));
//        System.out.println(Arrays.toString(a));
        System.out.println("First Element: " + a[0]);
        System.out.println("Last Element: " + a[a.length - 1]);
    }
    static int divisorSum(int n) {
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            if (n % i == 0)
                sum += i;
        }
        return sum;
    }

    //count words in camel case
    static int camelcase(String s) {
        String regexSensitive = "[A-Z]";
        int count = 1;
        for (int i = 0; i < s.length(); i++) {
            if (String.valueOf(s.charAt(i)).matches(regexSensitive)) {
                count++;
            }
        }
        return count;
    }
    //remove chars match near
    static String superReducedString(String s) {
        if (s.isEmpty()) {
            return "Empty String";
        }

        int preIndex = 0;
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(preIndex) == s.charAt(i)) {
//                remove duplicate
                String after =  s.substring(0, preIndex) + s.substring(preIndex + 2);
                System.out.println("debug: string after remove duplicate: " +after);
                return superReducedString(after);
            } else {
                preIndex = i;
//                return superReducedString(s);
            }
        }
        return s;
    }
//
    static Stack stack = new Stack(); //LIFO: last in first out
    static Queue queue = new LinkedList(); // FIFO first in first out
//    compare 2 texts if char in stack and queue are same.
    static void pushCharacter(char ch) {
        stack.push(ch);
    }
    static char popCharacter() {
        return (char) stack.pop();
    }

    static void enqueueCharacter(char ch) {
        queue.add(ch);
    }

    static char dequeueCharacter() {
        return (char) queue.remove();
    }

    static int power(int n, int p) throws Exception {
        if (n < 0 || p < 0) {
            throw new Exception("this is illegal");
        }
        return (int) Math.pow(n, p);
    }
//    day 15: linked list
    static Node insert(Node head, int data) {
        if (head == null)
            head = new Node(data);
        else if (head.next == null){
            Node next = new Node(data);
            head.next = next;
        } else {
            insert(head.next, data);
        }
        return head;
    }
    static void display(Node head) {
        Node start = head;
        while(start != null) {
            System.out.print(start.data + " ");
            start = start.next;
        }
    }
    static void computeDifference(int[] elements) {
        int computeDifference =  Arrays.stream(elements).max().getAsInt() - Arrays.stream(elements).min().getAsInt();
        System.out.println(computeDifference);
    }
//    done
    static void findLargestHourClass(int[][] arr) {
        int max = sumHourClass(0, 0, arr);
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (i == 0 && j == 0)
                    continue;

                if (isHourClass(arr[i][j], i, j, arr)) {
                    int sum = sumHourClass(i, j, arr);
                    if (sum > max) {
                        System.out.println("debug max position{0}, {1}: " + i + j);
                        max = sum;
                    }
                }
            }
        }
        System.out.println(max);
    }

    private static int sumHourClass(int i, int j, int[][] arr) {
        int sum = 0;
        for (int k = i; k < i + 3; k++) {
            for (int l = j; l < j + 3; l++) {
                if (k != i + 1)
                    sum += arr[k][l];
                else if (l == j + 1)
                    sum += arr[k][l];
            }
        }
        return sum;
    }

    private static boolean isHourClass(int value, int i, int j, int[][] arr) {
        if (i + 2 < arr.length && j + 2 < arr[0].length) {
            return true;
        }
        return false;
    }


    //    binary number
    static void binaryNumberMax(int n) {
        String binary = Integer.toBinaryString(n);
        System.out.println(binary);
        StringBuilder max = new StringBuilder();
        String current = "";
        char One = '1';
        for (int i = 0; i < binary.length(); i++) {
            if (One == (binary.charAt(i))) {
                max.append(One);
            } else {
                max.setLength(0);
            }
            if (max.length() > current.length())
                current = max.toString();
        }
        System.out.println("max count 1: " +current.length());
    }
    //plusMinus
    static void plusMinus(int[] arr) {
        long countNumber = Arrays.stream(arr).filter( n -> n > 0).count();
        double numberPositive = (double) countNumber / arr.length;
        double numberNegative = Arrays.stream(arr).filter( n -> n < 0).count() / arr.length;
        double number0 = Arrays.stream(arr).filter( n -> n == 0).count() / arr.length;
        DecimalFormat df = new DecimalFormat("#0.000000");
        System.out.println(df.format(numberPositive));
        System.out.println(df.format(numberNegative));
        System.out.println(df.format(number0));
    }

    // big sum
    static long aVeryBigSum(long[] ar) {

        return Arrays.stream(ar).sum();
    }
//    compare-the-triplets
    static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
        List<Integer> rs = new ArrayList<>();
        int pointA = 0, pointB = 0;
        for (int i = 0; i < a.size(); i++) {
            if (a.get(i).compareTo(b.get(i)) > 0) {
                pointA ++;
            } else if (a.get(i).compareTo(b.get(i)) < 0) {
                pointB ++;
            }
        }
        rs.add(pointA);
        rs.add(pointB);
        return rs;
    }
}
