package fairy.tail.problem;

import java.util.ArrayList;

public class Problem {

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        boolean b = is_shifted("abcdeghijk", "cdeghijka");
        long end = System.currentTimeMillis();
        System.out.println(b);
        System.out.println((end ));
        System.out.println(start);
    }

//    plan study: done
    public static boolean is_shifted(String a, String b) {
        int position = a.indexOf(b.charAt(0));
        String end = a.substring(0, position);
        String start = a.substring(position);
        System.out.println("string c = "+start + "--end: :"+end);
        String answers = start + end;
        if (answers.equals(b)) {
            return true;
        }
        return false;
    }

}
