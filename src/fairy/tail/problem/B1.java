package fairy.tail.problem;

import java.util.*;
import java.util.stream.Collectors;

/**
 * passed 81/83 cases. Failed case below with correct answer: 11
 * DFS algorithm but i don't know
 */
public class B1 {

    public static void main(String[] args) {
        String[] arrays = {"Hung", "Giang", "Khiem", "Thuc", "Chuyen", "Thuy", "Huong", "Huy", "Chung", "zkzkzk"};
        arrays = new String[]{"e","tpgynpylqbyqjaf","svkgfmpgftxjjrcxxsog","bxypbbrlckiolfwpqgsoc","kwnelumrnnsryjdeppanuqbsu"};
        List<String> listString = Arrays.stream(arrays).sorted( (s1, s2) -> {
           return s2.length() - s1.length();
        }).collect(Collectors.toList());

        System.out.println("source: "+listString);
        int rs = solution(listString);
        System.out.println("Result is: "+rs);
//        String test = "Kaikei";
//        int num = 5;
//        test = test(test, num);
//        System.out.println(test + "---test--"+ num);
    }

    public static int solution(List<String> input) {
        List result = new ArrayList();
        int maxLength = 0;
        for (int i = 0; i < input.size(); i++) {
            System.out.println("chain before: "+input.get(i));
            String chain = null;
            if (isUniqueString(input.get(i))) {
                chain = mergeChain(input.get(i), 0, input);
            }
            if (chain != null) {
                System.out.println("chain after: "+chain);
                result.add(chain);
                maxLength = Math.max(chain.length(), maxLength);
//                if (chain.length() > maxLength)
//                    maxLength = chain.length();
            }
        }
        System.out.println("Result is: "+result.toString());
        return maxLength;
    }

    private static String mergeChain(String builder, int i, List<String> input) {
        if (i >= input.size())
            return builder;

        String chain = "";
        for (int j = i; j < input.size(); j++) {
            if (!isDuplicateChar(builder, input.get(j)) && isUniqueString(input.get(j)) ) {
//                need find string has max length
                builder += input.get(j);
//                if (j + 1 < input.size()) {
                    chain = mergeChain(builder, 0, input);
//                } else {
//                    chain = builder;
//                }
                System.out.println("merge success: "+chain);
            } else {
                chain = builder;
            }
        }
        return chain;
    }

    private static boolean isUniqueString(String s) {
        Set check = new HashSet();
        for (int i = 0; i < s.length(); i++) {
            check.add(s.charAt(i));
        }
        System.out.println("debug: string: "+s+ "_^_^size check "+check.size() + "-- size s: "+s.length());
        if (check.size() == s.length())
            return true;
        else
            return false;
    }

    private static boolean isDuplicateChar(String chain, String check) {
        for (int i = 0; i < chain.length(); i++) {
            if (check.indexOf(chain.charAt(i)) > -1) {
                return true;
            }
        }
        return false;
    }

    private static String test(String a, int i) {
        a += a;
        i *= i;
        return a;
    }

}
