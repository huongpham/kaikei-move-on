package fairy.tail.problem;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Solution {

//    Task 1
    public int solution(int[] A) {
//        Set test = new HashSet();
//        test.add(new Integer(1));
//        test.add(new Integer(1));
//        System.out.println("set test: "+test.toString());
//        user java 8
//        AtomicInteger min = new AtomicInteger(1);
//        Arrays.stream(A).forEachOrdered( p -> {
//            if (p > 0 && min.get() == p) {
//                System.out.println(min + "--- "+p);
//                min.getAndIncrement();
//                return;
//            }
//        });
//        return min.get();
        int N = A.length;
        System.out.println(N + "--length");
        Set set = new HashSet();
        List<Integer> list = Arrays.stream(A).boxed().collect(Collectors.toList());
        System.out.println(list);
        list.forEach( p -> {
            if (p > 0) {
                set.add(p);
            }
        });
        System.out.println(set.toString());
        AtomicInteger min = new AtomicInteger(1);
        Arrays.asList(N).stream().forEach(q -> {
            System.out.println("what item: "+q);
            if (!set.contains(q)) {
                min.set(q);
                return;
            }
        });
        System.out.println("min is: "+min.get());
        return min.get();
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
//        int range = 1000000;
//        int[] A = new int[range];
//        int step = 5;
//        int temp;
//        for (int i = 0; i < range; i++) {
//            if (i > range - 10) {
//                temp = i + step;
//            } else {
//                temp = i;
//            }
//            A[i] = temp;
//        }
        int[] A = new int[]{1, 3, 6, 4, 1, 2};
        List<Integer> list = Arrays.stream(A).boxed().collect(Collectors.toList());
        System.out.println(Arrays.toString(A));
        System.out.println(solution.solution(A));
        Integer[] spam = new Integer[] { 1, 2, 3 };
        System.out.println(Arrays.asList(A).get(0));
//        a Giang
        int input[] = {1, 2, 4, -7, -8};
        Arrays.sort(input);
        int findNumber = 0;

        boolean finded = false;
        for (int i = 0; i < input.length - 1; i++) {
            if (input[i + 1] - input[i] > 1) {
                if (input[i] < 0) {
                    if (i < input.length - 2) {
                        if (input[i + 2] > 1) {
                            if (input[i + 1] > 1) {
                                findNumber = 1;
                                finded = true;
                                break;
                            }
                        }
                    }
                } else {
                    findNumber = input[i] + 1;
                    finded = true;
                    break;
                }
            }
        }
        if (!finded) {
            if (input[input.length - 1] >= 0)
                findNumber = input[input.length - 1] + 1;
            else
                findNumber = 1;
        }
        System.out.println("find number = " + String.valueOf(findNumber));

    }
    private static int getNumber(List<Integer> inputList) {
        int minNumber = inputList.stream().mapToInt(v -> v).min().orElseThrow(NoSuchElementException::new);
        if (minNumber < 0) {
            minNumber = 1;
        }
        while (true) {
            if (!inputList.contains(minNumber)) {
                return minNumber;
            }
            minNumber++;
        }
    }
}
