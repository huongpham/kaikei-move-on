package fairy.tail.problem;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class B5 {
    public static void main(String[] args) {
        Map phoneBook = new HashMap();
        phoneBook.put("sam", 99912222);
        phoneBook.put("tom", 11122222);
        phoneBook.put("harry", 122999333);
        System.out.println(phoneBook.get("sam"));
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        for(int i = 0; i < n; i++){
            String name = in.next();
            int phone = in.nextInt();
            // Write code here
            phoneBook.put(name, phone);

        }
        while(in.hasNext()){
            String s = in.next();
            // Write code here
            if (phoneBook.containsKey(s)) {
                System.out.println(s +"=" +phoneBook.get(s));
            } else {
                System.out.println("Not found");
            }
        }

        in.close();
    }
}
