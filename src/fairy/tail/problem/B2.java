package fairy.tail.problem;

public class B2 {

    public static void main(String[] args) throws InterruptedException {
        int n = 10;
        System.out.println((n & (n-1)) == 0 );
        System.out.println(n & 8);
        swapOpt(5, 6 );
        Thread thread = new Thread();
        thread.join();
    }

//    xor faster than others
    public static void swapOpt(int a, int b) {
        a = a ^ b;
//        System.out.println(a + "--- b: "+b);
        b = b ^ a ;
//        System.out.println(a + "--- b: "+b);
        a = a ^ b;
//        System.out.println(a + "--- b: "+b);
    }


}
