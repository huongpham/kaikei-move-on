package fairy.tail.problem;

import java.util.Scanner;

public class B3 {

    public static void main(String[] args) {
        int[][] a =  {{40,12,15,37,33,11,45,13,25,3},
                {37,35,15,43,23,12,22,29,46,43},
                {44,19,15,12,30,2,45,7,47,6},
                {48,4,40,10,16,22,18,36,27,48},
                {45,17,36,28,47,46,8,4,17,3},
                {14,9,33,1,6,31,7,38,25,17},
                {31,9,17,11,29,42,38,10,48,6},
                {12,13,42,3,47,24,28,22,3,47},
//                {38,23,26,3,23,27,14,40,15,22},
                {8,46,20,21,35,4,36,18,32,3}};
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        printMatrix(a);
        System.out.println("Result matrix after we rotate 90 by clockwise");
//        printMatrix(solution(a, x));
//        int[][] rs = rollingStone(a, x);
        printMatrix(rollingStone(a, x));
    }

    private static void printMatrix(int[][] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static int[][] rollingStone(int[][] stone, int nSteep) {
        if (nSteep == 0)
            return stone;
        int M = stone.length;
        int N = stone[0].length;
        System.out.println("matrix size: " +M + "x" +N);

        int[][] temp = new int[N][M];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                temp[i][j] = stone[M - j - 1][i];
            }
        }
        nSteep--;
        return rollingStone(temp, nSteep);
    }


    private static void setMatrix(int[][] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                a[i][j] = j;
            }
        }
    }
}
