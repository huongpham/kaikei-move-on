package fairy.tail.practice;

import java.io.*;

public class DeserializationNewInstanceExample {

    public static void main(String[] args) throws ClassNotFoundException, IOException {
        File file = File.createTempFile("student", "txt");
        serialize(file);
        deserialize(file);
    }

    private static void serialize(File file) throws IOException {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            StudentSerializable student = new StudentSerializable(1, "gpcoder");
            oos.writeObject(student);
            oos.flush();
            System.out.println("Serialized: "+student.hashCode());
        }
    }

    private static void deserialize(File file) throws IOException, ClassNotFoundException {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            StudentSerializable student = (StudentSerializable) ois.readObject();
            System.out.println(student);
            System.out.println("Deserialized: "+student.hashCode());
        }
    }

}
