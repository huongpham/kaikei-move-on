package fairy.tail.practice;

import java.lang.reflect.Field;

/**
 * use reflection
 */
public class NewInstanceExample {

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchFieldException {
        Class clazz = Class.forName("fairy.tail.practice.Employee");

        Employee employee = (Employee) clazz.newInstance();
        employee.setId(1);
        employee.setName("gpcoder");
        System.out.println("employee: "+employee);
//        way 2
        Field field = Employee.class.getDeclaredField("id");
        field.setAccessible(true);
        System.out.println("value now: "+field.get(employee));
        field.setInt(employee, 100);
        System.out.println("after value changed: " + field.get(employee));
    }
}
