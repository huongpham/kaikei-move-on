package fairy.tail.collection;

import java.util.*;

public class Collections {

    public static void main(String[] args) {
        Hashtable<Integer,String> ht=new Hashtable<Integer,String>();
        ht.put(101,"Vijay");
        ht.put(101," ajay");
        ht.put(102,"Ravi");
        ht.put(103,"Rahul");
        System.out.println("-------------Hash table--------------");
        for (Map.Entry m:ht.entrySet()) {
            System.out.println(m.getKey()+" "+m.getValue());
        }
        Set setHt = ht.entrySet();
        setHt.forEach(k -> System.out.println("value set: "+k));
        String hieu = "hieu";
        String hieu2 = "hieu";
        System.out.println("compare obj: " + hieu.hashCode()+ "--"+hieu2.hashCode());
        System.out.println(hieu == hieu2);

        //----------------hashmap--------------------------------
        HashMap<Integer,String> hm=new HashMap<Integer,String>();
        hm.put(100,"Amit");
        hm.put(104,"Amit");  // hash map allows duplicate values
        hm.put(101,"Vijay");
        hm.put(102,"Rahul");
        System.out.println("-----------Hash map-----------");
        for (Map.Entry m:hm.entrySet()) {
            System.out.println(m.getKey()+" "+m.getValue());
        }
        Hashtable hashtable = new Hashtable();
        hashtable.put(1, "Huong");
        hashtable.put(2, "Pham");
        hashtable.forEach( (k, v) -> System.out.println(k + "---" + v));
        Calendar calendar = Calendar.getInstance();

        Vector vector = new Vector(2);
        vector.add(2);
        vector.add('s');
        vector.add("hieu");
        for (Object o : vector) {
            System.out.println(o);
        }
        System.out.println("size vector changed: "+vector.capacity());
//        vector is synchronized but Arraylist is not synchronized
        ArrayList test = new ArrayList(2);
        test.add(4);
        test.add("333");
        test.add("Hương");
        test.add(222l);
        test.stream().forEach(item -> System.out.println(item));
        System.out.println(test.size());
    }
}
