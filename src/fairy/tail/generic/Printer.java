package fairy.tail.generic;


public class Printer {

    /**
     *    Method Name: printArray
     *    Print each element of the generic array on a new line. Do not return anything.
     *    @param A generic array
     **/

    // Write your code here
    public static <T> void printArray(T[] A) {
//        Arrays.stream(A).forEach(i -> System.out.println(i));
        for (T i: A) {
            System.out.println(i);
        }
    }

}
